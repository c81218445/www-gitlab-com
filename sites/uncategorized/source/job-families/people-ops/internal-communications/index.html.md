---
layout: job_family_page
title: "Internal Communications"
---

## Levels

### Internal Communications Associate

The Internal Communications Associate reports to the Senior Manager, Internal Communications.

#### Internal Communications Associate Job Grade

The Internal Communications Associate is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Internal Communications Associate Responsibilities

* Develop and execute proactive and strategic communication initiatives, programs, and events in collaboration with global teams across GitLab
* Own the internal editorial calendar, through vehicles such as our company newsletter, manager communication, executive videos and/or other communications channels
* Partner closely with internal stakeholders and E-Group to develop change management communications with clarity and empathy
* Have an eye and ear to the ground across GitLab team members and teams with the goal of maximizing the team member experience and to ensure GitLab remains a place where all team members belong and are included
* Be a shining example for GitLab’s culture, infusing GitLab’s mission, culture, and values into everything we do

#### Internal Communications Associate  Requirements

* Relevant communications experience
* Self-motivated and ability to work as a Manager of one
* Excellent writing, verbal, and listening skills 
* Adaptable and can think quickly on your feet
* Know how and when to take initiative to get projects done by masterfully using all possible resources
* Supremely organized and have fine-tuned project and program management skills
* Ability to use GitLab
* Aligns to GitLab's Values

### Internal Communications Program Manager

The Internal Communications Program Manager reports to the Senior Manager, Internal Communications.

#### Internal Communications Program Manager Job Grade

The Internal Communications Program Manager is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Internal Communications Program Manager Responsibilities

* Develop and execute proactive and strategic communication initiatives, programs, and events in collaboration with global teams across GitLab
* Own the internal editorial calendar and be the chief storyteller, whether it’s through our company newsletter, manager communication, executive videos and/or other communications channels
* Partner closely with internal stakeholders and E-Group to develop change management communications with clarity and empathy
* Have an eye and ear to the ground across GitLab team members and teams with the goal of maximizing the team member experience and to ensure GitLab remains a place where all team members belong and are included
* Be a shining example for GitLab’s culture, infusing GitLab’s mission, culture, and values into everything we do

#### Internal Communications Program Manager Requirements

* Relevant communications experience
* Self-motivated and ability to work as a Manager of one
* Proven track record of delivering internal communications that are representative of company values and culture in a global or international setting
* Uncanny ability to develop relationships and work with team members at all levels, including executive leadership, across a wide variety of teams and departments
* Excellent writing, verbal, and listening skills 
* Adaptable and can think quickly on your feet
* Know how and when to take initiative to get projects done by masterfully using all possible resources
* Supremely organized and have fine-tuned project and program management skills
* Ability to use GitLab
* Aligns to GitLab's Values

### Senior Internal Communications Manager

The Senior Internal Communications Manager reports to the Senior Manager, Internal Communications.

#### Senior Internal Communications Manager Job Grade

The Senior Internal Communications Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Internal Communications Manager Responsibilties

* Develop and execute proactive and strategic communication initiatives, programs, and events in collaboration with global teams across GitLab
* Own the internal editorial calendar and be the chief storyteller, whether it’s through our company newsletter, manager communication, executive videos and/or other communications channels
* Partner closely with internal stakeholders and E-Group to develop change management communications with clarity and empathy
* Have an eye and ear to the ground across GitLab team members and teams with the goal of maximizing the team member experience and to ensure GitLab remains a place where all team members belong and are included
* Be a shining example for GitLab’s culture, infusing GitLab’s mission, culture, and values into everything we do
* Once internal communications team grows, partner and provide dedicated communications support to certain E-Group members and leaders of their functions (think People Business Partner type model)

#### Senior Internal Communications Manager Requirements

* Relevant communications experience
* Demonstrate the capability to drive initiatives, develop thoughtful communication strategies, and deliver results at a senior level
* Self-motivated and ability to work as a Manager of one
* Supremely organized and have fine-tuned project and program management skills
* Proven track record of delivering internal communications that are representative of company values and culture in a global or international setting
* Uncanny ability to develop relationships and work with team members at all levels, including executive leadership, across a wide variety of teams and departments
* Excellent writing, verbal, and listening skills 
* Adaptable and can think quickly on your feet
* Know how and when to take initiative to get projects done by masterfully using all possible resources
* Ability to use GitLab
* Aligns to GitLab's Values

### Staff Internal Communications Manager

The Staff Internal Communications Manager reports to the Senior Manager, Internal Communications.

#### Staff Internal Communications Manager Job Grade

The Staff Internal Communications Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Staff Internal Communications Manager Responsibilities

* Extends that of the Senior Internal Communications Manager responsibilities
* Partner with the Internal Communications leader in providing ‘internal PR’ support, including crisis communications
* Once internal communications team grows, partner and provide dedicated communications support to certain E-Group members and leaders of their functions (think HR Business partner type model)


#### Staff Internal Communications Manager Requirements

* Extends that of the Senior Internal Communications Manager requirements
* Out-of-the-box thinker who is both creative and strategic
* Intellectual curiosity and want to learn anything and everything

### Senior Manager, Internal Communications 

The Senior Manager, Internal Communications reports to the [Senior Director, Talent Brand and Enablement](job-families/people-ops/talent-brand-and-talent-acquisition-enablement/#senior-director-of-talent-brand--talent-acquisition-enablement).

#### Senior Manager, Internal Communications Job Grade

The Senior Manager, Internal Communications is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Manager, Internal Communications Responsibilities

* Drive a three-year internal communications roadmap
* Lead, hire, and coach the growing  internal communications team
* Influence and collaborate with E-Group and members of the Marketing and People teams to plan and improve company-wide team member communications including Slack, issue comments, and other asynchronous communication, Handbook education, company calls, breakout calls, group conversations, AMAs, etc.
* Help drive CEO internal communications through supporting Group Conversations, Contribute and priority initiatives through crafting messaging and creating materials
* Help moderate internal communications to ensure GitLab values are upheld as well as compliance with our code of conduct
* Provide ‘internal PR’ support, including crisis communications
* Act as a communications consultant to team members to help develop campaigns for internal programs or initiatives (e.g. GitLab referral program)
* Curate and publish key internal news (e.g., “Here are the 3 things you need to know” digest) on a weekly basis
* Partner closely with the Talent Brand Manager to ensure integrity and cohesion between our external talent brand and internal team member experience and effectively articulate GitLab’s culture and value proposition
* Collaborate with key stakeholders to develop specific communication plans (e.g., work with People Business Partners and People Operations on Engagement Survey communication)
* Plan, communicate, and coordinate celebratory occasions, company communication initiatives, volunteer activities, and other team member engagement opportunities

#### Senior Manager, Internal Communications Requirements

* Relevant communications experience
* Demonstrate the capability to drive initiatives, develop thoughtful communication strategies, and deliver results at a senior level.
* Self motivated and works as a team of one.
* Demonstrated capability to deliver internal communications that are representative of company values and culture in a global or international setting
* Enthusiasm for leading internal communications in a unique, fully remote environment where email and face-to-face meetings are not the standard communication media
* Excellent narration and writing skills
* Ability to navigate cultural differences and build global but locally relevant solutions
* Experienced social and communication skills (verbal and written), across all levels with significant experience in executive level communications
* Excellent organizational skills, time management, and priority setting
* Deadline oriented, and able to work in a fast-paced environment with ever-changing priorities
* Ability to work as a Manager of one, as well as work collaboratively
* Proficiency in communication tools such as Slack, Zoom, and Google Workspace is a must
* You share and can role model our values, and work in accordance with those values
* Ability to use GitLab

### Director, Internal Communications

The Director, Internal Communications reports to the [Senior Director, Talent Brand and Enablement](job-families/people-ops/talent-brand-and-talent-acquisition-enablement/#senior-director-of-talent-brand--talent-acquisition-enablement).

#### Director, Internal Communications Job Grade

The Director, Internal Communications is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director, Internal Communications Responsibilities

* Design and execute an internal communications strategy that connects every GitLab team member to our mission, culture, values, and priorities.
* Own and drive the CEO’s internal storytelling and narrative building
* Drive a three-year internal communications roadmap
* Provide strategic internal communications counsel to E-Group 
* Lead, hire, and coach the growing  internal communications team
* Manage planning and execution of Quarterly All Hands (GitLab Assembly)
* Instill a data-driven mindset and robust two-way communication approach to internal communication effectiveness
* Establish operating rhythm with key business functions to ensure key projects and priorities are effectively communicated
* Lead ‘internal PR’ support, including crisis communications
* Develop innovative ways of engaging team members across various communications channels

#### Director, Internal Communications Requirements

* Relevant Experience with internal and/or executive communications 
* Experience building a high-growth, scaling global internal communications function 
* Outstanding leadership skills and someone who takes initiative, makes hard decisions, and is accountable
* Proven track record of working closely with an executive leadership team 
* Demonstrated capability to drive initiatives, develop thoughtful communication strategies, and deliver results at a senior level
* Willingness to roll sleeves up and work on complex issues and crises on deadline
* High emotional intelligence, matched with the ability to execute
* Enthusiasm for leading internal communications in a unique, fully remote environment where email and face-to-face meetings are not the standard communication media
* Bias for action with an ability to work in a fast-paced environment with ever-changing priorities
* Highly collaborative and team-oriented
* Adapt to change quickly and be a change agent for others
* Know and advocate on behalf of GitLab team members
* Proficiency in communication tools such as Slack, Zoom, and Google Workspace is a must
* You share and can role model our values, and work in accordance with those values
* Ability to use GitLab

## Performance Indicators

* [Percent of sent Slack messages that are not DMs > 25%](h/handbook/communication/#avoid-direct-messages)
* [Team member engagement survey score](/handbook/people-group/engagement/)
* [Team member retention](/handbook/people-group/people-group-metrics/#team-member-retention)
* Ad hoc feedback from team members on specific programs and initiatives

## Career Ladder

The next step in the Internal Communications job family is to move to a senior leader job family of which we do not yet have defined at GitLab.

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Qualified candidates will be invited to schedule a 30 minute screening call with one of our recruiters
* Next, qualified candidates will meet with the hiring manager
* Next, qualified candidates will be asked to supply a writing sample and complete a short writing assignment (e.g. up to 500 words, which will be detailed by the hiring manager)
* Next, qualified candidates will meet with 2 - 4 team members.

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
